from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_protect
from django.utils import simplejson
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from models import Slika, Komentar
from forms import UploadForm


def index(request):
    sve_slike = Slika.objects.all().order_by('-id')
    paginator = Paginator(sve_slike, 16)
    stranica = request.GET.get('stranica')    

    try:
        slike_stranica = paginator.page(stranica)
    except PageNotAnInteger:
        slike_stranica = paginator.page(1)
    except EmptyPage:
        slike_stranica = paginator.page(paginator.num_pages)

    context = {'sve_slike': slike_stranica}
    return render(request, 'slikapp/index.html', context)

class UploadCreate(CreateView):
    model = Slika
    context_object_name = 'datoteka'
    template_name = 'slikapp/upload.html'
    form_class = UploadForm
    success_url = reverse_lazy('slikapp:index')

def slika(request, id):
    slika = get_object_or_404(Slika, pk=id)
    return render(request, 'slikapp/slika.html', {'slika': slika, 'komentari': slika.komentar_set.all()})

@csrf_protect
def komentar(request):
    results = ''
    try:
        if request.method == u'POST':
            if request.POST.has_key(u'pk') and request.POST.has_key(u'tekst'):
                pk = int(request.POST[u'pk'])
                slika = Slika.objects.get(pk=pk)
                kom = slika.komentar_set.create(tekst=request.POST[u'tekst'])
                results = '<li><small>'+ kom.vrijeme.strftime("%c") +'</small><p>'+ kom.tekst +'</p></li>'
    except Slika.DoesNotExist:
        pass
    return HttpResponse(results)
