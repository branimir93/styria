from django.conf.urls import patterns, url
from slikapp import views

urlpatterns = patterns('',
	url(r'^slika/(?P<id>\d+)/$', views.slika, name='slika'),
	url(r'^upload', views.UploadCreate.as_view(), name='upload'),
	url(r'^komentar', views.komentar, name='komentar'),
	url(r'^$', views.index, name='index'),
)
