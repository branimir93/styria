from django.db import models

class Slika(models.Model):
	datoteka = models.ImageField(upload_to='slike')
	opis = models.CharField(max_length=200)

	def __unicode__(self):
		return self.opis

class Komentar(models.Model):
	tekst = models.TextField()
	slika = models.ForeignKey(Slika)
	vrijeme = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.tekst
